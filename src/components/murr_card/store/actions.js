import axios from "axios";

import {
  STATUS_200_OK,
  STATUS_204_NO_CONTENT,
} from "../../../utils/http_response_status.js";
import { handlerErrorAxios } from "../../../utils/helpres.js";

import * as type from "./type.js";

const MURR_CARDS_START_PAGE = 1;

export default {
  async [type.MURR_CARD_FETCH_LIST](
    { commit, getters },
    { page = MURR_CARDS_START_PAGE }
  ) {
    try {
      const jwtToken = getters.accessToken_getters;

      let headers = jwtToken ? {Authorization: `Bearer ${jwtToken}`} : {}
      const { data, status } = await axios.get(`/api/murr_card/?page=${page}`, {
        // todo: Fix - send to global instance of axios
        headers: headers,
      });

      if (status === STATUS_200_OK) {
        commit(type.MURR_CARDS_APPEND, data);

        return {
          success: true,
          data,
          existsNextPage: data.next !== null,
        };
      }

      return { success: false, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },
  async [type.MURR_CARD_MY_LIKES](
    { commit, getters },
    { page = MURR_CARDS_START_PAGE }
  ) {
    try {
      const jwtToken = getters.accessToken_getters;

      const { data, status } = await axios.get(
        `/api/murr_card/my_likes/?page=${page}`,
        {
          // todo: Fix - send to global instance of axios
          headers: {
            Authorization: `Bearer ${jwtToken}`,
          },
        }
      );

      if (status === STATUS_200_OK) {
        commit(type.MURR_CARDS_APPEND, data);

        return {
          success: true,
          data,
          existsNextPage: data.next !== null,
        };
      }

      return { success: false, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [type.MURR_CARD_LIKED]({ getters }, { murrID }) {
    try {
      const jwtToken = getters.accessToken_getters;

      const { data, status } = await axios.post(
        `/api/murr_card/${murrID}/like/`,
        {},
        {
          // todo: Fix - send to global instance of axios
          headers: {
            Authorization: `Bearer ${jwtToken}`,
          },
        }
      );

      return { success: true, status, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [type.MURR_CARD_UNLIKED]({ getters }, { murrID }) {
    try {
      const jwtToken = getters.accessToken_getters;

      const { data, status } = await axios.post(
        `/api/murr_card/${murrID}/dislike/`,
        {},
        {
          // todo: Fix - send to global instance of axios
          headers: {
            Authorization: `Bearer ${jwtToken}`,
          },
        }
      );

      return { success: true, status, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [type.MURR_CARD_SUBSCRIBE]({ commit, getters }, { murrenID }) {
    try {
      const jwtToken = getters.accessToken_getters;

      const { data, status } = await axios.post(
        `/api/murren/${murrenID}/subscribe/`,
        {},
        {
          // todo: Fix - send to global instance of axios
          headers: {
            Authorization: `Bearer ${jwtToken}`,
          },
        }
      );
      
      if (status === STATUS_200_OK) {
        commit(type.MURR_CARDS_CHANGE_SUBSCRIBER_STATUS, {
          murrenID: murrenID,
          isSubscriber: data.is_subscriber,
        });

        return { success: true, data };
      }

      return { success: false, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [type.MURR_CARD_FETCH_ONE](
    { commit, getters },
    { murrID }
  ) {
    try {
      const jwtToken = getters.accessToken_getters;

      let headers = jwtToken ? {Authorization: `Bearer ${jwtToken}`} : {}
      const { data, status } = await axios.get(`/api/murr_card/${murrID}/`, {
        // todo: Fix - send to global instance of axios
        headers: headers,
      });

      if (status === STATUS_200_OK) {
        commit(type.MURR_CARD_SET, data);

        return { success: true, data };
      }

      return { success: false, data };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [type.MURR_CARD_DELETE]({ commit, getters }, { murrID }) {
    try {
      const token = getters.accessToken_getters;

      const { data, status } = await axios.delete(`/api/murr_card/${murrID}/`, {
        // todo: Fix - send to global instance of axios
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if (status === STATUS_204_NO_CONTENT) {
        commit(type.MURR_CARD_CLEAR);

        return { success: true, message: "Мурр успешно удален!" };
      }

      return { success: false, message: data.detail };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },
};
