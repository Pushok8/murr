import { optionsDefault } from "./index.js";

export default {
  open(state, payload) {
    state.isOpen = true;
    state.payload = payload;
    state.transition = payload.transition;
    state.options = { ...state.options, ...optionsDefault };

    if (payload.options) {
      state.options = { ...state.options, ...payload.options };
    }
  },
  close(state) {
    state.isOpen = false;
    state.payload = null;
    state.transition = null;
  },
};
