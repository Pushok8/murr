from django.contrib.auth import get_user_model

from murr_websocket.consumers.base import BaseMurrWebSocketConsumer

Murren = get_user_model()


class MurrChatConsumer(BaseMurrWebSocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scope['murr_ws_type'] = 'murr_chat'

    async def connect(self):
        await super().connect()
        await self.channel_layer.group_add(self.scope['murr_ws_name'], self.channel_name)
        await self._group_send(f"Подключение к мурр_чату - {self.scope['murr_ws_name']}", gan='connect_to_murr_chat')

    async def disconnect(self, code):
        await self._group_send(f"Отключение от мурр_чата - {self.scope['murr_ws_name']}",
                               gan='disconnect_MurrChatConsumer')
        await self.channel_layer.group_discard(self.scope['murr_ws_name'], self.channel_name)
        await super().disconnect(code=code)
