from django.conf import settings

from rest_framework import serializers
from djoser.serializers import UserCreateSerializer

from .models import Murren, TrustedRegistrationEmail


class MurrenSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()
    subscribers_count = serializers.ReadOnlyField()
    is_subscriber = serializers.SerializerMethodField()

    class Meta:
        model = Murren
        fields = ('id', 'username', 'email', 'murren_avatar', 'avatar', 'subscribers_count', 'is_subscriber')
        read_only_fields = ('avatar', 'subscribers_count', 'is_subscriber', )

    def get_avatar(self, obj):
        return f'{settings.BACKEND_URL}{obj.murren_avatar.url}'
    
    def get_is_subscriber(self, obj):
        user = self.context.get('request').user
        if user.is_authenticated:
            return obj.in_subscribers(user)
        return False


class CustomUserCreateSerializer(UserCreateSerializer):

    def validate(self, attrs):
        user_email_service = attrs.get("email").split('@')[1]
        email_service_is_trusted = TrustedRegistrationEmail.objects.filter(service_domain=user_email_service).exists()
        if not email_service_is_trusted:
            raise serializers.ValidationError(
                {"email": ["Your email service is not allowed for register"]}
            )
        return super().validate(attrs)
